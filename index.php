<?php
/**
 * Plugin Name: Gutenberg Examples
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin  how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

defined( 'ABSPATH' ) || exit;

require_once '01-basic/index.php';
require_once '01-basic-esnext/index.php';
require_once '02-stylesheets/index.php';
require_once '03-editable/index.php';
require_once '03-editable-esnext/index.php';
require_once '04-controls/index.php';
require_once '04-controls-esnext/index.php';
require_once '05-recipe-card/index.php';
require_once '05-recipe-card-esnext/index.php';
require_once '06-inner-blocks/index.php';
require_once '06-inner-blocks-esnext/index.php';
require_once '07-slotfills-esnext/index.php';
require_once '08-block-supports-esnext/index.php';
require_once '09-code-data-basics-esnext/index.php';
require_once 'format-api/index.php';
require_once 'plugin-sidebar/plugin-sidebar.php';
require_once 'meta-block/meta-block.php';
require_once '10-dynamic-block/index.php';
